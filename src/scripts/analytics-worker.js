/**
 * A web worker dedicated to batching and sending click event data to the server.
 * It collects events sent from the main thread and batches them. When the batch
 * reaches a specified size, it sends the batch to the server for analytics processing.
 */

/** @type {Array<Object>} */
let eventsBatch = [];

/**
 * Sends a batch of events to the server.
 * This function takes an array of event objects, converts them to a JSON string,
 * and sends them to a specified endpoint using the POST method. It logs the response
 * or any error encountered during the fetch operation.
 *
 * @param {Array<Object>} batch - The batch of events to be sent to the server.
 */
function sendToServer(batch) {
  // Define the endpoint and configuration for the fetch request
  fetch("http://localhost:3000/analytics/user", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(batch),
  })
    .then((response) => response.json())
    .then((data) => console.log("Batch sent successfully:", data))
    .catch((error) => console.error("Error sending batch:", error));
}

/**
 * Event listener for messages received from the main thread.
 * Adds each received event to a batch. When the batch size reaches 5,
 * it sends the batch to the server using `sendToServer` function and then resets the batch.
 */
self.addEventListener("message", (e) => {
  eventsBatch.push(e.data); // Add the received event data to the batch

  // Check if the batch size has reached the threshold
  if (eventsBatch.length === 5) {
    sendToServer(eventsBatch); // Send the batch to the server
    eventsBatch = []; // Reset the batch after sending
  }
});
